#ifndef POTTS_H
#define POTTS_H

#include <stdlib.h>
#include <stdio.h>
#include "random_park.h"
#include "math.h"
#include <ctime>

using namespace std;
using namespace SPPARKS_NS;

//Adds a pin to a particular site
// retuns 0 if no pin was added (failure)
// returns 1 if the pin was added (success)
int addPin(lattice &potts_mat, int site) {
	if (potts_mat.atoms[site].ip[2] == 1) {
		return 0; //a pin already exists here
	}
	
	int site_value = potts_mat.atoms[site].ip[0];
	int neighbor, neigh_value, neigh_phasevalue;
//	int if_boundary = 0;
	for (int i = 0; i < 26; i++) {
		neighbor = potts_mat.atoms[site].neighbors[i];
		neigh_phasevalue = potts_mat.atoms[neighbor].ip[2];
		if (neigh_phasevalue == 1) {
			return 0; //prevent pins from overlapping
		}
	}
	
	for (int i = 0; i < 26; i++) {
		neigh_value = potts_mat.atoms[neighbor].ip[0];
		if (neigh_value != site_value) {
//			if_boundary = 1; //pin would be in the boundary
			potts_mat.atoms[site].ip[2] = 1;
			potts_mat.atoms[site].ip[1] = 25;
			potts_mat.atoms[site].ip[0] = 100;
			for (int j = 0; j < 26; j++) {
				potts_mat.atoms[potts_mat.atoms[site].neighbors[j]].ip[2] = 1;
				potts_mat.atoms[potts_mat.atoms[site].neighbors[j]].ip[1] = 25;
				potts_mat.atoms[potts_mat.atoms[site].neighbors[j]].ip[0] = 100;
			}
			
			return 1;
		}		
	}
	return 0;
}

void addPins(lattice &potts_mat, float volume_fraction) {
	int limit = 50000; // break loop after this number of iterations
	float volume_fraction_perpin = 26.0 / float(potts_mat.nop);
	int number_of_pins = ceil(volume_fraction/volume_fraction_perpin);
	cout << "--ADDING PINS...\n";
	cout << " <> Volume fraction of pins = " << volume_fraction_perpin << endl;
	cout << " <> Number of pins to add = " << number_of_pins << endl;
	
	int count_added_pins = 0;
	int pin_site = -1;
	int add_bool = 0;
	int count_iteration = 0;
	while (count_added_pins < number_of_pins) {
		pin_site = rand() % (potts_mat.nop);
		add_bool = addPin(potts_mat, pin_site);
		if (add_bool == 1) {
//			cout << "pin added" << endl;
			count_added_pins++;
		}
		else {
//			cout << "pin not added" << endl;
		}
		if(count_iteration == limit) {
			break;
		}
		count_iteration++;
	}
	
}



void generatePottsMatrix(lattice &potts_mat) {
	potts_mat.populateAtoms();
	potts_mat.generateCoordinates();
	potts_mat.populateNeighbors();
	potts_mat.addIPproperty("energy");
	potts_mat.addIPproperty("phase");
    potts_mat.addIPproperty("counts");

	int id_it = 1;
	for (int i = 0; i < potts_mat.nop; i++) {
		potts_mat.atoms[i].ip[0] = id_it;  //id->first initialized in lattice.cpp
		//ip[1]->energy initialized in lattice.cpp
		potts_mat.atoms[i].ip[2] = 0; //phase
//		potts_mat.atoms[i].ip[3] = 0; //count
		id_it += 1;
	}
}

void flip_event(lattice &matrix, float temp, int i, RandomPark &randgen) {
	int E_i = 0;
	int E_f = 0;
	int unique[26];
	for (int p = 0; p < 26; p++) {
		unique[p] = -1;
	}
	if (matrix.atoms[i].ip[2] == 1) {
		return;
	}
	
	int current_value = matrix.atoms[i].ip[0];
	
	int j,m,value;
	int nevent = 0;
	//START --FROM SPPARKS
	for (j = 0; j < 26; j++) {
		value = matrix.atoms[matrix.atoms[i].neighbors[j]].ip[0];
		if (value == current_value) continue;
		for (m = 0; m < nevent; m++)
		  if (value == unique[m]) break;
		if (m < nevent) continue;
		unique[nevent++] = value;
	}
//	cout << "UNIQUE " << unique[0] << endl;
	if (nevent == 0) return;
	int iran = (int) (nevent*randgen.uniform());
	if (iran >= nevent) iran = nevent-1;
//	spin[i] = unique[iran];
	
	//END
	int new_value = unique[iran];
	
/*	cout << "\n\n---------------bla " <<  i << " " <<  matrix.atoms[i].ID << " " << matrix.atoms[i].ip[0] << endl;

	for (int tempi = 0; tempi < 26; tempi++) {
		cout << matrix.atoms[matrix.atoms[i].neighbors[tempi]].ID << " ";
	}
	cout << "\n" << "--" << "\n";
	for (int tempi = 0; tempi < 26; tempi++) {
		cout << matrix.atoms[matrix.atoms[i].neighbors[tempi]].ip[0] << " ";
	}

	cout << "\n" << "--" << "\n";
	for (int tempt = 0; tempt < 26; tempt++) {
		cout << unique[tempt] << " ";
	}
	cout << "\n" << "--" << "\n";
	cout <<  "chkvalue: ";*/
	
	int check_value;
	for (int ni = 0; ni < 26; ni++) {
		check_value = matrix.atoms[matrix.atoms[i].neighbors[ni]].ip[0];
		if (current_value != check_value) {
			E_i++;
		}
		if (new_value != check_value) {
			E_f++;
		}
//		cout << check_value << " ";
	}
//	cout << "\n" << "--" << "\n";
	matrix.atoms[i].ip[1] = E_i;
//	cout << "Current value: " << current_value << endl;
//	cout << "New value: " << new_value << endl;
//	cout << E_i << " " << E_f << endl;
	
//	if (E_i != 0) { 
	float P;
	if (E_f > E_i) {
		P = exp((E_i-E_f)/float(temp));
	}
	else {
		P = 1;
	}
	float R = randgen.uniform();
	
//	cout << "RP: " <<  R << " " << P << " " << endl;
	if (R <= P) {
		matrix.atoms[i].ip[0] = new_value;
		matrix.atoms[i].ip[1] = E_f;
//		cout << "FLIP! " << E_f - E_i;
//		}
	} 

}

void run_potts(lattice &matrix, int mcs, float temp=1.0) {
	clock_t begin;
	clock_t end;
	int steps_ct = 0;
	int mcs_ct = 0;
	int sms_ct = 0;
	RandomPark randgen(7541);
	while (mcs_ct < mcs) {
		begin = clock();
		sms_ct = 0;
		//rand_vec = np.random.permutation(nop)
		while (sms_ct < matrix.nop) {
			//int rand_val = randgen.irandom(matrix.nop)-1;
			int rand_val = rand() % (matrix.nop);
			flip_event(matrix, temp, rand_val, randgen);
			matrix.atoms[rand_val].ip[3] += 1;
			sms_ct += 1;
			steps_ct += 1;
		}

		
		cout << " <>MCS # " << mcs_ct << "\n";
		end = clock();
		double elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
		cout << "   +MCS time: " << elapsed_secs << "\n";
		
		
		mcs_ct += 1;
	}
}

#endif