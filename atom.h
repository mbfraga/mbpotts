#ifndef ATOMS_H
#define ATOMS_H
#include <vector>
#include <string>
#include <iostream>



using namespace std;

class atom {
	public:
		int *value;
		vector<int> ip;		//list of integer properties (index 0 = orientation)
		vector<float> fp;	//list of float properties
		 int xloc;
		 int yloc;
		 int zloc;
		 int ID;
		
		vector<int> neighbors;
		vector<vector<int> > neighbor_vlocs;
		atom();
		virtual ~atom();
		
		virtual void printInformation(int=0);
		virtual void writeNeighbor(int, int, int);
};

#endif