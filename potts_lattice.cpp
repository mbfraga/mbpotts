#include <stdlib.h>
#include <stdio.h>
#include "math.h"
#include "random_park.h"

#include "potts_lattice.h"

#ifndef MISC_TOOLS_H
#include "misc_tools.h"
#endif





void latticePotts::flip_event(float temp, int i) {
	int E_i = 0;
	int E_f = 0;
	int unique[26];
	for (int p = 0; p < 26; p++) {
		unique[p] = -1;
	}
	
	if (atoms[i].ip[2] == 0) {

		
		int current_value = atoms[i].ip[0];
		
		int j,m,value;
		int nevent = 0;
		//START --FROM SPPARKS
		for (j = 0; j < 26; j++) {
			value = atoms[atoms[i].neighbors[j]].ip[0];
			if (value == current_value) continue;
			for (m = 0; m < nevent; m++)
			  if (value == unique[m]) break;
			if (m < nevent) continue;
			unique[nevent++] = value;
		}
	//	cout << "UNIQUE " << unique[0] << endl;
		if (nevent == 0) return;
		int iran = (int) (nevent*randgen.uniform());
		if (iran >= nevent) iran = nevent-1;
	//	spin[i] = unique[iran];
		
		//END
		int new_value = unique[iran];
		
	/*	cout << "\n\n---------------bla " <<  i << " " <<  matrix.atoms[i].ID << " " << matrix.atoms[i].ip[0] << endl;

		for (int tempi = 0; tempi < 26; tempi++) {
			cout << matrix.atoms[matrix.atoms[i].neighbors[tempi]].ID << " ";
		}
		cout << "\n" << "--" << "\n";
		for (int tempi = 0; tempi < 26; tempi++) {
			cout << matrix.atoms[matrix.atoms[i].neighbors[tempi]].ip[0] << " ";
		}

		cout << "\n" << "--" << "\n";
		for (int tempt = 0; tempt < 26; tempt++) {
			cout << unique[tempt] << " ";
		}
		cout << "\n" << "--" << "\n";
		cout <<  "chkvalue: ";*/
		
		int check_value;
		for (int ni = 0; ni < 26; ni++) {
			check_value = atoms[atoms[i].neighbors[ni]].ip[0];
			if (current_value != check_value) {
				E_i++;
			}
			if (new_value != check_value) {
				E_f++;
			}
	//		cout << check_value << " ";
		}
	//	cout << "\n" << "--" << "\n";
		atoms[i].ip[1] = E_i;
	//	cout << "Current value: " << current_value << endl;
	//	cout << "New value: " << new_value << endl;
	//	cout << E_i << " " << E_f << endl;
		
	//	if (E_i != 0) { 
		float P;
		if (E_f > E_i) {
			P = exp((E_i-E_f)/float(temp));
		}
		else {
			P = 1;
		}
		float R = randgen.uniform();
		
	//	cout << "RP: " <<  R << " " << P << " " << endl;
		if (R <= P) {
			atoms[i].ip[0] = new_value;
			atoms[i].ip[1] = E_f;
	//		cout << "FLIP! " << E_f - E_i;
	//		}
		} 

	}
}

latticePotts::latticePotts() {
	
}

latticePotts::~latticePotts() {
	
}

void latticePotts::preparePotts(int xv, int yv, int zv) {
	xdim = xv;
	if (yv == -1) {
		ydim = xv;
	}
	else {
		ydim = yv;
	}
	if (zv == -1) {
		zdim = xv;
	}
	else {
		zdim = zv;
	}
	nof_iproperties = 0; 
	nof_fproperties = -1;
	
	nop = xdim*ydim*zdim;
	cout << "\n***STARTING LATTICE APPLICATION***\n";
	cout << "--LATTICE DIMENSIONS: " << endl;
	cout << " <> x = " << xdim << "\n <> y = " << ydim
		<< "\n <> z = " << zdim << endl;
	cout << "--NUMBER OF POINTS (nop): " << nop << endl;
	
	ipropertyNames.push_back("value");
	randgen = 9924;
	snapshot_step = 0;
}



void latticePotts::create_potts_run(string output_folder, string runname) {
	dircreate(output_folder + "/" + runname);
	dircreate(output_folder + "/" + runname + "/orientation", 1);
	dircreate(output_folder + "/" + runname + "/energy", 1);
	dircreate(output_folder + "/" + runname + "/phase", 1);
	dircreate(output_folder + "/" + runname + "/count", 1);
	
	dirclean(output_folder + "/" + runname + "/orientation");
}

void latticePotts::generatePottsMatrix() {
	populateAtoms();
	generateCoordinates();
	populateNeighbors();
	addIPproperty("energy");
	addIPproperty("phase");
    addIPproperty("counts");

	int id_it = 1;
	for (int i = 0; i < nop; i++) {
		atoms[i].ip[0] = id_it;  //id->first initialized in lattice.cpp
		//ip[1]->energy initialized in lattice.cpp
		atoms[i].ip[2] = 0; //phase
//		potts_mat.atoms[i].ip[3] = 0; //count
		id_it += 1;
	}
}

void latticePotts::run_potts(int mcs, float temp=1.0) {
	clock_t begin;
	clock_t end;
	int steps_ct = 0;
	int mcs_ct = 0;
	int sms_ct = 0;

	while (mcs_ct < mcs) {
		begin = clock();
		sms_ct = 0;
		//rand_vec = np.random.permutation(nop)
		while (sms_ct < this->nop) {
			//int rand_val = randgen.irandom(matrix.nop)-1;
			int rand_val = rand() % (this->nop);
			flip_event(temp, rand_val);
			this->atoms[rand_val].ip[3] += 1;
			sms_ct += 1;
			steps_ct += 1;

		}

		
		cout << " <>MCS # " << mcs_ct << "\n";
		end = clock();
		double elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
		cout << "   +MCS time: " << elapsed_secs << "\n";
		
		
		mcs_ct += 1;
	}
	snapshot_step += 1;
}

















//Adds a pin to a particular site
// retuns 0 if no pin was added (failure)
// returns 1 if the pin was added (success)
int latticePotts::addPin(int site) {
	if (this->atoms[site].ip[2] == 1) {
		return 0; //a pin already exists here
	}
	
	int site_value = this->atoms[site].ip[0];
	int neighbor, neigh_value, neigh_phasevalue;
//	int if_boundary = 0;
	for (int i = 0; i < 26; i++) {
		neighbor = this->atoms[site].neighbors[i];
		neigh_phasevalue = this->atoms[neighbor].ip[2];
		if (neigh_phasevalue == 1) {
			return 0; //prevent pins from overlapping
		}
	}
	
	for (int i = 0; i < 26; i++) {
		neigh_value = this->atoms[neighbor].ip[0];
		if (neigh_value != site_value) {
//			if_boundary = 1; //pin would be in the boundary
			this->atoms[site].ip[2] = 1;
			this->atoms[site].ip[1] = 25;
			this->atoms[site].ip[0] = 100;
			for (int j = 0; j < 26; j++) {
				this->atoms[this->atoms[site].neighbors[j]].ip[2] = 1;
				this->atoms[this->atoms[site].neighbors[j]].ip[1] = 25;
				//this->atoms[this->atoms[site].neighbors[j]].ip[0] = 100;
			}
			
			return 1;
		}		
	}
	return 0;
}

void latticePotts::addPins(float volume_fraction) {
	int limit = 50000; // break loop after this number of iterations
	float volume_fraction_perpin = 26.0 / float(this->nop);
	int number_of_pins = ceil(volume_fraction/volume_fraction_perpin);
	cout << "--ADDING PINS...\n";
	cout << " <> Volume fraction of pins = " << volume_fraction_perpin << endl;
	cout << " <> Number of pins to add = " << number_of_pins << endl;
	
	int count_added_pins = 0;
	int pin_site = -1;
	int add_bool = 0;
	int count_iteration = 0;
	while (count_added_pins < number_of_pins) {
		pin_site = rand() % (this->nop);
		add_bool = addPin(pin_site);
		if (add_bool == 1) {
//			cout << "pin added" << endl;
			count_added_pins++;
		}
		else {
//			cout << "pin not added" << endl;
		}
		if(count_iteration == limit) {
			break;
		}
		count_iteration++;
	}
	
}



