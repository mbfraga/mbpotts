#ifndef POTTS_LATTICE_H
#define POTTS_LATTICE_H

#include <stdlib.h>
#include <stdio.h>

#ifndef LATTICE_H
#include "lattice.h"
#endif
#include "random_park.h"

using namespace std;
using namespace SPPARKS_NS;

class latticePotts : public lattice {
public:
	
	int snapshot_step;
	//latticePotts(int a1,int a2 = -1,int a3 = -1) : lattice(a1,a2,a3) { };
	//using lattice::lattice(int,int=-1,int=-1);
	explicit latticePotts();
	virtual void preparePotts(int,int=-1,int=-1);
	RandomPark randgen = 7717;
//		explicit latticePotts(int,int=-1,int=-1);
	virtual ~latticePotts();
	virtual void create_potts_run(string, string);
	virtual int addPin(int);
	virtual void addPins(float);
	virtual void generatePottsMatrix();
	virtual void flip_event(float,int);
	virtual void run_potts(int, float);
		 
};

#endif