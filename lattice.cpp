#include "string.h"
#include "math.h"
#include "stdlib.h"
#include <iostream>
#include <fstream>
#include <stdio.h>

#ifndef LATTICE_H
#include "lattice.h"
#endif



using namespace std;

lattice::~lattice() {
	cout << "\n***ENDING LATTICE APPLICATION***\n";
}

/*lattice::lattice(int xv, int yv, int zv) {
	xdim = xv;
	if (yv == -1) {
		ydim = xv;
	}
	else {
		ydim = yv;
	}
	if (zv == -1) {
		zdim = xv;
	}
	else {
		zdim = zv;
	}
	nof_iproperties = 0; 
	nof_fproperties = -1;
	
	nop = xdim*ydim*zdim;
	cout << "\n***STARTING LATTICE APPLICATION***\n";
	cout << "--LATTICE DIMENSIONS: " << endl;
	cout << " <> x = " << xdim << "\n <> y = " << ydim
		<< "\n <> z = " << zdim << endl;
	cout << "--NUMBER OF POINTS (nop): " << nop << endl;
	
	ipropertyNames.push_back("value");
}*/

lattice::lattice() {
	
}

void lattice::populateAtoms() {
	cout << "\n--Populating Atoms...";
	atoms.resize(nop);
//	vector<atom>::iterator it;
//	it = atoms.begin();
//	it = atoms.insert(it, nop);
	for (int i = 0; i < nop; i++) {
		atoms[i].ID = i;
	}
	
//	cout << greentext("[OK]") << endl;
}

void lattice::printAtoms() {
	cout << "\n--START: Printing Atoms\n";
	
	for (int i=0; i < nop; i++) {
		cout << "ID=" << atoms[i].ID << " ";
		cout << "x=" << atoms[i].xloc << " ";
		cout << "y=" << atoms[i].yloc << " ";
		cout << "z=" << atoms[i].zloc << " ";
		cout << endl;
	}
	
	cout << "\n--END: Printing Atoms" << endl;
}

void lattice::generateCoordinates() {
	cout << "\n--START: Generating Coordinates \n";
	int i = 0;
	for ( int zt = 0; zt < zdim; zt++) {
		for ( int yt = 0; yt < ydim; yt++) {
			for ( int xt = 0; xt < xdim; xt++) {
				atoms[i].xloc = xt;
				atoms[i].yloc = yt;
				atoms[i].zloc = zt;
				i += 1;
			}
		}
	}
	cout << "--END: Generating Coordinates" << endl;
}

void lattice::populateNeighbors() {
	cout << "\n--START: Populating Neighbors \n";
	for (int i = 0; i < nop; i++) {
		atoms[i].writeNeighbor(xdim, ydim, zdim);
	}
	
	cout << "--END: Populating Neighbors" << endl;
}

void lattice::addIPproperty(string property_name) {
	cout << "\n--ADDING PROPERTY: " << property_name << "...";
	nof_iproperties += 1;
	ipropertyNames.push_back(property_name);
	for (int i = 0; i < nop; i++) {
		atoms[i].ip.push_back(-1);
	}
	cout << "OK\n";
}

void lattice::addFPproperty(string property_name) {
	cout << "\n--ADDING PROPERTY: " << property_name << "...";
	nof_fproperties += 1;
	fpropertyNames.push_back(property_name);
	for (int i = 0; i < nop; i++) {
		atoms[i].fp.push_back(-1.0);
	}
	cout << "OK\n";
}

void lattice::writeOutput(string filename) {
	cout << "\n--WRITING OUTPUT: " << "...";
	ofstream outputfile;
	outputfile.open(filename.c_str());
	
	if (outputfile.is_open()) {
	
	outputfile << "#HEADER: ID x y z ori properties...";
	for (int i = 0; i < nop; i++) {
		outputfile << i << " " << atoms[i].xloc << " " << atoms[i].yloc << " " << atoms[i].zloc;
		outputfile << " " << atoms[i].ip[0] << " ";
		for (int n = 1; n < nof_iproperties; n++) {
			if (n >= 1) {
				outputfile << " " << atoms[i].ip[n];
			}
		}
		outputfile << "\n";
	}
	outputfile.close();
	}
	else {
		cout << "Unable to open file: " << filename << "\n";
	}
}


void lattice::writeIPlane(string filename, int z_depth, string property_name) {
	cout << "\n--WRITING OUTPUT: " << "...";
	ofstream outputfile;
	outputfile.open(filename.c_str());
	
	if (outputfile.is_open()) {
		
		if (property_name == "all") {
			cout << "NOTHING HERE YET" << endl;
		}
		else {
			int start_index = z_depth*(xdim*ydim);
			int property_index = -1;
			for (int i = 0; i < (int)ipropertyNames.size(); i++) {
				if (property_name == ipropertyNames[i]) {
					property_index = i;
				}
			}
			
			int i_count = start_index;
			for (int yd = 0; yd < ydim; yd++) {
				for ( int xd = 0; xd < xdim; xd++) {
					outputfile << atoms[i_count].ip[property_index] << " ";
					i_count++;
				}
				outputfile << "\n";
			}
			
			outputfile.close();	
		}
	}
	
}

void lattice::calculateEnergy() {
	int energy = 0;
	for (int i = 0; i < nop; i++) {
		for (int j = 0; j < 26; j++) {
			if (atoms[atoms[i].neighbors[j]].ip[0] != atoms[i].ip[0] ) {
				energy += 1;
			}
		}

	}
	system_energy.push_back(energy);
}

void lattice::calculateClusters() {
	vector<int> cluster_list(nop,0);
	int unique_clusters = 0;
	int cluster_sum = 0;
	int value = 0;
	
	for (int i = 0; i < nop; i++) {
		value = atoms[i].ip[0];
		cluster_list[value]++;
	}
	
	for (int i = 0; i < nop; i++) {
		if (cluster_list[i] != 0) {
			unique_clusters++;
			cluster_sum += cluster_list[i];
		}
	}
	
	float cluster_size = (float)cluster_sum/(float)unique_clusters;
	
	
	clusters.push_back(cluster_size);
}