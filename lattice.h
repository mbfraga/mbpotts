#ifndef LATTICE_H
#define LATTICE_H

#include <vector>
#include <string>
#include <iostream>

#include "atom.h"

using namespace std;

//This is the main 3-dimensional lattice that will contain all the points (atoms)
class lattice {
	public:
		int xdim;
		int ydim;
		int zdim;
		int nop; //number of points
		vector<atom> atoms;
		vector<string> ipropertyNames;
		vector<string> fpropertyNames;
		int nof_iproperties; 
		int nof_fproperties;
		
		vector<int> system_energy;
		vector<float> clusters;
		
		explicit lattice();
		virtual ~lattice();
		
		virtual void populateAtoms();
		virtual void printAtoms();
		virtual void generateCoordinates();
		virtual void populateNeighbors();
		virtual void addIPproperty(string);
		virtual void addFPproperty(string);
		
		virtual void calculateEnergy();
		virtual void calculateClusters();
		
		virtual void writeOutput(string);
		virtual void writeIPlane(string, int, string = "value");
};

#endif