#include "string.h"
#include "math.h"
#include "stdlib.h"
#include <iostream>
#include <stdio.h>
#include "atom.h"

using namespace std;

atom::atom() {
	xloc = -1;
	yloc = -1;
	zloc = -1;
	ID = -1;

	
	ip.push_back(-1);
	value = &ip[0];
	neighbors.resize(26,-1);
	
	neighbor_vlocs.resize(26);
	for (int i = 0; i < 26; i++) {
		neighbor_vlocs[i].resize(3, -1);
	}
}


void atom::writeNeighbor(int xdim, int ydim, int zdim) {
		 int x = xloc;
		 int y = yloc;
		 int z = zloc;
		 int xv = 0;
		 int yv = 0;
		 int zv = 0;
		 int xi = 0;
		 int yi = 0;
		 int zi = 0;
		int virtualx = 0;
		int virtualy = 0;
		int virtualz = 0;
		
		 int dim = 3;
		 int neigh_ct = 0;
		
		for (zi = 0; zi < dim; zi++) {
			zv = z + zi -1;
			virtualz = zv;
			if (zv >= zdim) {
				zv -= zdim;
			}
			else if (zv < 0) {
				zv += zdim;
			}
			
			for (yi = 0; yi < dim; yi++) {
				yv = y + yi -1;
				virtualy = yv;
				if (yv >= ydim) {
					yv -= ydim;
				}
				else if (yv < 0) {
					yv += ydim;
				}
			
				for (xi = 0; xi < dim; xi++) {
					xv = x + xi -1;
					virtualx = xv;
					if (xv >= xdim) {
						xv -= xdim;
					}
					else if (xv < 0) {
						xv += xdim;
					}
					
					if (!(xv-x == 0 && yv-y == 0 && zv-z == 0)) {
						neighbor_vlocs[neigh_ct][0] = virtualx;
						neighbor_vlocs[neigh_ct][1] = virtualy;
						neighbor_vlocs[neigh_ct][2] = virtualz;
						
						neighbors[neigh_ct] = (zv*(xdim*ydim) + yv*xdim + xv);
						neigh_ct += 1;
					}
				}
			}

		} 
}

void atom::printInformation(int neigh_info) {
	cout << "\nPRINTING INFORMATION OF ATOM# " << ID << "\n";
	cout << "x = " << xloc << "; ";
	cout << "y = " << yloc << "; ";
	cout << "z = " << zloc << "\n";
	
	cout << "value = " << value << "\n";
	
	cout << "number of i_properties = " << ip.size() << "\n";
	for (unsigned int in = 0; in < ip.size(); in++) {
		cout << " --" << in << "| " << ip[in] << endl;
	}
	cout << "number of f_properties = " << fp.size() << "\n";
	if (fp.size() > 0) {
		for (unsigned int fn = 0; fn < fp.size(); fn++) {
			cout << " --" << fn << "| " << fp[fn] << endl;
			
		}	
	}

	
	cout << "Neighbor Information:\n";
	
	if (neigh_info == 1) {
		for (int i = 0; i < 26; i++) {
			cout << " <> " << i << " : "  << neighbors[i] << "  (";
			cout << "x = " << neighbor_vlocs[i][0] << "; ";
			cout << "y = " << neighbor_vlocs[i][1] << "; ";
			cout << "z = " << neighbor_vlocs[i][2] << " )\n";
		}
	}


}

atom::~atom() {
}
