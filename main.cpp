//#include "main.h"
#include "string.h"
#include "math.h"
#include "stdlib.h"
#include <iostream>
#include <fstream>
#include <stdio.h>

#ifndef POTTS_LATTICE_H
#include "potts_lattice.h"
#endif 

#ifndef POTTS_H
#include "potts.h"
#endif



int main(int argc, char **argv)
{
	string sLine = "";
	ifstream myfile;

	//GHETO PARSE---
	myfile.open("input");
	getline(myfile, sLine);
	getline(myfile, sLine);
	string runname = sLine;
	getline(myfile, sLine);
	getline(myfile, sLine);
	string working_directory = sLine;
	getline(myfile, sLine);
	getline(myfile, sLine);
	int sizex = atoi(sLine.c_str());
	getline(myfile, sLine);
	getline(myfile, sLine);
	int sizey = atoi(sLine.c_str());
	if (sizey == -1) { sizey = sizex;}
	getline(myfile, sLine);
	getline(myfile, sLine);
	int sizez = atoi(sLine.c_str());
	if (sizey == -1) { sizey = sizex;}
	getline(myfile, sLine);
	getline(myfile, sLine);
	int mcs = atoi(sLine.c_str());
	getline(myfile, sLine);
	getline(myfile, sLine);
	int steps = atoi(sLine.c_str());
	getline(myfile, sLine);
	getline(myfile, sLine);
	float temp = atof(sLine.c_str());
	getline(myfile, sLine);
	getline(myfile, sLine);
	float volfrac = atof(sLine.c_str());
	getline(myfile, sLine);
	getline(myfile, sLine);
	int pin_attime = atof(sLine.c_str());
	getline(myfile, sLine);
	getline(myfile, sLine);
	int pin_atsize = atof(sLine.c_str());
	//---------------
	
	
	int pinned_bool = 1;
	latticePotts A;
	A.preparePotts(sizex,sizey,sizez);
	A.create_potts_run(working_directory, runname);
	A.generatePottsMatrix();
	
//	A.atoms[13].printInformation();
//	A.writeIPlane(working_directory + "test1/Step_000", 0);
	char timestep[5];
	A.calculateEnergy();
	A.calculateClusters();
	for (int i = 0; i <= steps; i++) {
		cout << "STEP # " << i << endl;
		sprintf(timestep, "%03i", A.snapshot_step);
		cout << " <>Writting output: " << working_directory + runname 
					  + "/orientation/Step_" + timestep << endl;
		A.writeIPlane(working_directory + runname 
					  + "/orientation/Step_" + timestep + ".out", 0, "value");
		A.writeIPlane(working_directory + runname 
					  + "/energy/Step_" + timestep + ".out", 0, "energy");
		A.writeIPlane(working_directory + runname 
					  + "/phase/Step_" + timestep + ".out", 0, "phase");
		A.writeIPlane(working_directory + runname 
					  + "/count/Step_" + timestep + ".out", 0, "count");


		A.run_potts(mcs, temp);
		
		A.calculateEnergy();
		A.calculateClusters();
	
		string filename = working_directory + runname + "/energy_clusters";
		ofstream outputfile;
		outputfile.open(filename.c_str());
		if (outputfile.is_open()) {
			for (unsigned int n = 0; n < A.clusters.size(); n++) {
				outputfile << n << " " << A.system_energy[n] << " " << pow(A.clusters[n], (1.0/3.0)) << "\n";
			}
		}
		outputfile.close();
		
		if ( i == pin_attime && pinned_bool == 1) {
			A.addPins(volfrac);
			pinned_bool = 0;
			cout << " >>>PINS ADDED at time " << pin_attime << endl;
		}
		if (A.clusters[(int)A.clusters.size()-1] >= pin_atsize && pinned_bool == 1) {
			A.addPins(volfrac);
			pinned_bool = 0;
			cout << " >>>PINS ADDED at cluster size " << pin_atsize << endl;
			cout << "DEBUG: " << A.clusters.size()-1 << " " << A.clusters[(int)A.clusters.size()-1] << endl;
		}
	}
	
/*	for (int a = 0; a < A.nop; a++) {
		cout << "\n";
		cout << A.atoms[a].ID << " ";
		cout << A.atoms[a].ip[0] <<  " " << A.atoms[a].ip[3] <<"\n";
	}*/
//	A.addPins(0.05);
	
/*	for (int i = 0; i < steps; i++) {
		cout << "STEP # " << i << endl;
		A.run_potts(mcs, 2);
		sprintf(timestep, "%03i", A.snapshot_step);
		cout << " <>Writting output: " << working_directory + runname 
					  + "/orientation/Step_" + timestep << endl;
		A.writeIPlane(working_directory + runname 
					  + "/orientation/Step_" + timestep + ".out", 0, "value");
		A.writeIPlane(working_directory + runname 
					  + "/energy/Step_" + timestep + ".out", 0, "energy");
		A.writeIPlane(working_directory + runname 
					  + "/phase/Step_" + timestep + ".out", 0, "phase");
		A.writeIPlane(working_directory + runname 
					  + "/count/Step_" + timestep + ".out", 0, "count");

		A.calculateEnergy();
		A.calculateClusters();
		
		string filename = working_directory + runname + "/energy_clusters";
		ofstream outputfile;
		outputfile.open(filename.c_str());
		if (outputfile.is_open()) {
			for (unsigned int n = 0; n < A.clusters.size(); n++) {
				outputfile << n << " " << A.system_energy[n] << " " << pow(A.clusters[n], (1.0/3.0)) << "\n";
		}
	}
	outputfile.close();
	}*/
	
	
	
	cout << "\n <> Energy progress: ";
	for ( unsigned int n = 0; n < A.system_energy.size(); n++) {
		cout << A.system_energy[n] << " ";
	}
	cout << endl;
	
	cout << "\n <> Cluster progress: ";
	for (unsigned int n = 0; n < A.clusters.size(); n++) {
		cout << A.clusters[n] << " ";
	}
	cout << endl;
	

/*	sprintf(timestep, "%03i", steps+1);
	A.writeIPlane(working_directory + runname 
				  + "/orientation/Step_" + timestep + ".out", 0, "value");
	A.writeIPlane(working_directory + runname 
				  + "/energy/Step_" + timestep + ".out", 0, "energy");
	A.writeIPlane(working_directory + runname 
				  + "/phase/Step_" + timestep + ".out", 0, "phase");
	A.writeIPlane(working_directory + runname 
				  + "/count/Step_" + timestep + ".out", 0, "count");*/
	
	
	string filename = working_directory + runname + "/energy_clusters";
	ofstream outputfile;
	outputfile.open(filename.c_str());
	if (outputfile.is_open()) {
		for (unsigned int n = 0; n < A.clusters.size(); n++) {
			outputfile << n << " " << A.system_energy[n] << " " << pow(A.clusters[n], (1.0/3.0)) << "\n";
		}
	}
	outputfile.close();
	
	
	cout << endl << "Step count: "  << A.snapshot_step << endl;
//	A.atoms[13].printInformation();
/*	
	for (int i = 0; i < 26; i++) {
		cout << A.atoms[0].neighbor_vlocs[i][0] << " ";
		cout << A.atoms[0].neighbor_vlocs[i][1] << " ";
		cout << A.atoms[0].neighbor_vlocs[i][2] << "\n";
	}
	return 0; */
//	A.atoms[5].writeNeighbor(size,size,size);
//	A.atoms[5].printInformation();
//	cout << A.atoms[5].ip[0]<< endl;
}
