#ifndef MISC_TOOLS_H
#define MISC_TOOLS_H

#include <stdio.h>
#include <string>
#include <fstream>
#include <iostream>
#include <sstream>
//File Delete
#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>
//

using namespace std;

//Convert a string to a  number <int> or <float>
template <typename T>
T StringToNumber ( const string &Text ) 
{
      istringstream ss(Text);
         T result;
            return ss >> result ? result : 0;
} 


//Create Directory in argument
static void dircreate(string directory, int indent_num = 0)
{
         int i_n = 0;
         while ( i_n < indent_num)
         {
            printf("    ");
            i_n +=1;
         }
         printf("\033[0;36m|+\033[0;33m");
         printf("Creating %s ...",directory.c_str()); //DEBUG
      
      if(mkdir(directory.c_str(),0777) == -1)
      {
         printf("\033[1;34m[EXISTS]\033[0m\n");
               //      cout << directory << " directory exists" << endl
      }
      else
      {
//         cout << greentext("[OK]") << endl;
      }
}

//Clean directory in argument (directory, bool to delete dir, 
//                               int indent num)
static void dirclean(string directory, int delete_dir_bool = 0, int indent_num = 0)
{
   DIR *dpdf;
   struct dirent *ent;
   struct stat filestat;
   dpdf = opendir(directory.c_str());
   string filepath;

//   cout << endl << "--------------" << endl;
//   cout << "Cleaning " << directory << endl << endl;
   if (dpdf != NULL)
   {
      while ((ent = readdir (dpdf)) != NULL)
      {
         filepath = directory + "/" +   ent->d_name;



         if (stat( filepath.c_str(), &filestat )) continue;
         if (S_ISDIR( filestat.st_mode ))         continue;

      //   int i_n = 0;
      //   while ( i_n < indent_num)
      //   {
      //      printf("    ");
      //      i_n +=1;
      //   }
      //   printf("\033[0;36m|-\033[0;33m");
      //   printf("Deleting %s ...",filepath.c_str()); //DEBUG
        
          if (remove(filepath.c_str()) != 0)
         {
//             printf("\033[1;31m[FAIL]\033[0m\n");
         }
         else
         {
//            printf("\033[1;32m[OK]\033[0m\n");
         }
      }
   }
//   cout << "--------------" << endl;
}




#endif

